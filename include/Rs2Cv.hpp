#pragma once
#include <librealsense2/rs.hpp>
#include <opencv2/opencv.hpp>

namespace rs2cv {

/////////////////
// Parameters //
////////////////

/// Default value for enabling the `depth` stream
const bool DEFAULT_ENABLE_DEPTH = true;
/// Default value for enabling the `infra1` (left) stream
const bool DEFAULT_ENABLE_INFRA_LEFT = true;
/// Default value for enabling the `infra2` (right) stream
const bool DEFAULT_ENABLE_INFRA_RIGHT = true;
/// Default value for enabling the `color` stream
const bool DEFAULT_ENABLE_COLOR = true;
/// Default width of the `depth` frames
const int DEFAULT_WIDTH_DEPTH = 424;
/// Default height of the `depth` frames
const int DEFAULT_HEIGHT_DEPTH = 240;
/// Default framerate of the `depth` stream
const int DEFAULT_FRAMERATE_DEPTH = 6;
/// Default width of the `infra` frames
const int DEFAULT_WIDTH_INFRA = DEFAULT_WIDTH_DEPTH;
/// Default height of the `infra` frames
const int DEFAULT_HEIGHT_INFRA = DEFAULT_HEIGHT_DEPTH;
/// Default framerate of the `infra` streams
const int DEFAULT_FRAMERATE_INFRA = DEFAULT_FRAMERATE_DEPTH;
/// Default width of the `color` frames
const int DEFAULT_WIDTH_COLOR = 320;
/// Default height of the `color` frames
const int DEFAULT_HEIGHT_COLOR = 180;
/// Default framerate of the `color` stream
const int DEFAULT_FRAMERATE_COLOR = 6;

/// Format of the `depth` stream
const rs2_format FORMAT_DEPTH = RS2_FORMAT_Z16;
/// Format of the `infra1` (left) stream
const rs2_format FORMAT_INFRA_LEFT = RS2_FORMAT_Y8;
/// Format of the `infra2` (right) stream
const rs2_format FORMAT_INFRA_RIGHT = RS2_FORMAT_Y8;
/// Format of the `color` stream
const rs2_format FORMAT_COLOR = RS2_FORMAT_BGR8;

/// Index of the `infra1` (left) stream
const int INDEX_INFRA_LEFT = 1;
/// Index of the `infra2` (right) stream
const int INDEX_INFRA_RIGHT = 2;

/////////////
// Structs //
/////////////

/// A struct containing information about what streams are enabled
struct enabled_streams {
  /// Indicates whether `depth` stream is enabled
  bool depth = false;
  /// Indicates whether `infra1` (left) stream is enabled
  bool infra_left = false;
  /// Indicates whether `infra2` (right) stream is enabled
  bool infra_right = false;
  /// Indicates whether `color` stream is enabled
  bool color = false;
};

/// A struct containing all D400 frames represented as `cv::Mat`
struct cv_frameset {
  /// Frame from `depth` stream
  cv::Mat1s depth;
  /// Frame from `infra1` (left) stream
  cv::Mat1b infra_left;
  /// Frame from `infra2` (right) stream
  cv::Mat1b infra_right;
  /// Frame from `color` stream
  cv::Mat3b color;
};

///////////
// Class //
///////////

/// Class that extracts frames from a RealSense D400 device or rosbag recording
/// and converts them into `cv::Mat` structures.
class Rs2Cv {
private:
  /// A struct containing information about what streams are enabled
  enabled_streams enabled_streams_;
  /// The pipeline simplifies the user interaction with D400 device or rosbag
  /// recording
  rs2::pipeline pipeline_;

public:
  /// Constructor that does nothing
  Rs2Cv();
  /// Destructor that shuts the pipeline down
  ~Rs2Cv();
  /// Initiates `Rs2Cv` based on a rosbag recording. The next invocation of
  /// `Rs2Cv::wait_for_frames()` returns next set of recorded frames.
  /// # Arguments
  /// * `file_name`       - Absolute path to the rosbag recording
  /// * `repeat_playback` - Determines whether to repeat the playback or not
  void init_from_rosbag(const std::string &file_name,
                        const bool &repeat_playback = true);
  /// Initiates `Rs2Cv` based on a D400 device. Default
  /// device configuration is selected. The next invocation of
  /// `Rs2Cv::wait_for_frames()` returns next set of available frames.
  /// # Arguments
  /// * `serial` - The serial number of the device
  void init_device(const std::string &serial);
  /// Initiates `Rs2Cv` based on a D400 device. The next invocation of
  /// `Rs2Cv::wait_for_frames()` returns next set of available frames.
  /// # Arguments
  /// * `serial` - The serial number of the device
  /// * `config` - The device configuration to use
  void init_device(const std::string &serial, rs2::config &config);
  /// Returns the next available frames
  cv_frameset wait_for_frames();
  /// Returns the baseline between the `infra1` (left) and `infra2` (right)
  /// imaging sensors
  float get_baseline();
  /// Returns the focal length of the `infra1` (left) or `infra2` (right)
  /// imaging sensor
  /// # Arguments
  /// * `sensor_index` - The index of the sensor, i.e. 1 for left and 2 for
  /// right
  float get_infra_focal_length(const int &sensor_index = INDEX_INFRA_LEFT);

private:
  /// Determines what streams are enabled and overwrites `enabled_streams_`
  inline void determine_enabled_streams();
  /// Extracts a `depth` frame from an `rs2::frameset` and returs `cv::Mat`
  /// # Arguments
  /// * `rs2_frameset` - The frameset containing the frame
  inline void extract_depth_frame(const rs2::frameset &rs2_frameset,
                                  cv::Mat1s &output);
  /// Extracts a `infra1` (left) frame from an `rs2::frameset` and returs
  /// `cv::Mat`
  /// # Arguments
  /// * `rs2_frameset` - The frameset containing the frame
  inline void extract_infra_left_frame(const rs2::frameset &rs2_frameset,
                                       cv::Mat1b &output);
  /// Extracts a `infra2` (right) frame from an `rs2::frameset` and returs
  /// `cv::Mat`
  /// # Arguments
  /// * `rs2_frameset` - The frameset containing the frame
  inline void extract_infra_right_frame(const rs2::frameset &rs2_frameset,
                                        cv::Mat1b &output);
  /// Extracts a `color` frame from an `rs2::frameset` and returs `cv::Mat`
  /// # Arguments
  /// * `rs2_frameset` - The frameset containing the frame
  inline void extract_color_frame(const rs2::frameset &rs2_frameset,
                                  cv::Mat3b &output);
  /// Converts `rs2::frame` to `cv::Mat`
  /// # Arguments
  /// * `rs2_frameset` - The frame to convert
  /// * `mat_type`     - The `cv::type` of the frame
  inline void convert_frame_to_cv_mat(const rs2::frame &frame,
                                      const int &mat_type, cv::Mat &output);
};

} // namespace rs2cv