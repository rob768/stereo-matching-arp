#pragma once
#include <opencv2/opencv.hpp>
#include <vector>

namespace stereo_matcher {

/////////////////
// Parameters //
////////////////

/// The size of the sobel filter applied to the images
const uint8_t SOBEL_KERNEL_SIZE = 3;

/// The window size used for disparity computations, it must be an odd number
const uint8_t WINDOW_SIZE = 21;

/// The step size used during computing disparities
const uint8_t DISPARITY_STEP_SIZE = 1;
/// The number of pixels to consider the disparity for (to the left of the
/// pixel)
const uint16_t MAX_DISPARITY_LEFT = 50;
/// If defined, disparities are computed also to the right of the pixel,
/// applicable only for non-parallel (converging) camera setups.
// #define COMPUTE_DISPARITIES_TO_RIGHT
#ifdef COMPUTE_DISPARITIES_TO_RIGHT
/// The number of pixels to consider the disparity for (to the right of the
/// pixel)
const uint16_t MAX_DISPARITY_RIGHT = MAX_DISPARITY_LEFT / 2;
#endif

/// If defined, `Sum of Squared Differences` is used for cost computation
/// instead of `Sum of Absolute Differences`
// #define USE_SSD_COST
/// The minimum required cost for a disparity to be consider a valid match
const uint32_t MINIMUM_REQUIRED_COST = 30 * std::pow(WINDOW_SIZE, 2);
/// If defined, the current disparity is returned as result, if its cost is less
/// than `SATISFACTORY_COST`. Otherwise, all disparities in the valid range are
/// computed.
#define STOP_IF_SATISFACTORY_COST_FOUND
#ifdef STOP_IF_SATISFACTORY_COST_FOUND
/// If a cost is computed to be less than `SATISFACTORY_COST`, disparity
/// computations stop and the current pixel is returned for disparity
const int32_t SATISFACTORY_COST = std::pow(WINDOW_SIZE, 2);
#endif

/// Scalar factor for determining the resulting units of the depth image
/// * `1.0`     -> 1 unit = 1 m
/// * `100.0`   -> 1 unit = 1 cm
/// * `1000.0`  -> 1 unit = 1 mm
const float DEPTH_SCALE = 1000.0;

///////////
// Class //
///////////

/// Class that find correspondence between left and right images in a binocular
/// stereo setup. Disparity and depth are alligned to left image.
class StereoMatcher {
private:
  /// Multiplication of baseline, focal length and depth scale, which is stored
  /// to reduce the number of computations while computing depth from disparity
  uint32_t baseline_x_focal_length_;

public:
  /// Constructor that initiates `StereoMatcher`
  /// # Arguments
  /// * `baseline`     - The distance between the left and right cameras in m
  /// * `focal_length` - The common focal length of the left and right cameras
  /// in px
  StereoMatcher(const float &baseline, const float &focal_length);
  /// Computes disparity between the two images
  /// # Arguments
  /// * `img_left`  - A frame acquired from the left imaging sensor
  /// * `img_right` - A frame acquired from the right imaging sensor
  /// * `output`    - Output disparity
  void compute_disparity(cv::Mat1b &img_left, cv::Mat1b &img_right,
                         cv::Mat1s &output);
  /// Computes depth based on `disparity`
  /// # Arguments
  /// * `disparity` - The disparity between the left and right imaging sensors
  /// * `output`    - Output depth
  void compute_depth(const cv::Mat1s &disparity, cv::Mat1s &output);
  /// Computes depth between the two images. This function provides slightly
  /// better performance than the combination of `compute_disparity()` and the
  /// other overloaded`compute_depth()`. # Arguments
  /// * `img_left`  - A frame acquired from the left imaging sensor
  /// * `img_right` - A frame acquired from the right imaging sensor
  /// * `output`    - Output depth
  void compute_depth(cv::Mat1b &img_left, cv::Mat1b &img_right,
                     cv::Mat1s &output);

private:
  /// Applies preprocessing to both acquired frames
  /// # Arguments
  /// * `img_left`  - A frame acquired from the left imaging sensor
  /// * `img_right` - A frame acquired from the right imaging sensor
  inline void preprocess(cv::Mat1b &img_left, cv::Mat1b &img_right);
  /// Computes disparity for the reference pixel(row, col) in `img_left` in
  /// respect to `img_right`. This means that the pixel(row, col+disparity) in
  /// `img_right` is the input pixel's best match that minimizes the cost.
  /// # Arguments
  /// * `img_left`  - A frame acquired from the left imaging sensor
  /// * `img_right` - A frame acquired from the right imaging sensor
  /// * `row`       - Row of the pixel
  /// * `col`       - Column of the pixel
  inline uint16_t compute_pixel_disparity(const cv::Mat1b &img_left,
                                          const cv::Mat1b &img_right,
                                          const uint16_t &row,
                                          const uint16_t &col);
  /// Extracts a window defined by `row`, `col` and `WINDOW_SIZE`
  /// # Arguments
  /// * `img`    - The input image
  /// * `row`    - Row of the pixel
  /// * `col`    - Column of the pixel
  /// * `output` - Output window
  inline void extract_window(const cv::Mat1b &img, const int &row,
                             const int &col, cv::Mat1b &output);
#ifndef USE_SSD_COST
  /// Computes cost as `Sum of Absolute Differences`
  /// # Arguments
  /// * `window_left`  - A window acquired from the left imaging sensor used
  /// during the computation
  /// * `window_right` - A window acquired from the right imaging sensor used
  /// during the computation
  inline uint32_t compute_SAD_cost(const cv::Mat1b &window_left,
                                   const cv::Mat1b &window_right);
#else
  /// Computes cost as `Sum of Squared Differences`
  /// # Arguments
  /// * `window_left`  - A window acquired from the left imaging sensor used
  /// during the computation
  /// * `window_right` - A window acquired from the right imaging sensor used
  /// during the computation
  inline uint32_t compute_SSD_cost(const cv::Mat1b &window_left,
                                   const cv::Mat1b &window_right);
#endif
};

} // namespace stereo_matcher
