#pragma once
#include <librealsense2/rs.hpp>
#include <opencv2/opencv.hpp>

namespace utils {

/// Parses cli argument to `main()`
inline std::string parse_cli_argument(int argc, char *argv[]);
/// Prints information about cli usage and throwns an exception
inline void exception_invalid_argument();
/// Prints to STDOUT the type and serial of all connected devices
inline void print_connected_devices();
/// Prints an introduction to STDOUT
inline void show_intro_screen(const float &baseline, const float &focal_length);
/// Colorizes depth image of type `CV_16U`
/// # Arguments
/// * `input` - The image to colorize, which must be of type `CV_16U`
/// * `colormap` - The colormap preset to use, e.g. cv::COLORMAP_JET
inline void colorize(const cv::Mat1s &input, cv::Mat3b &output,
                     const int &colormap);
/// Turns values of input to 0 if mask is 0
inline void black_based_on_mask(const cv::Mat3b &input, cv::Mat3b &output,
                                const cv::Mat1s &mask);
/// Helper function that saves and shows an image
inline void imgshow(const cv::String &name, cv::InputArray input);

std::string parse_cli_argument(int argc, char *argv[]) {
  if (argc != 2) {
    exception_invalid_argument();
  }
  std::string argument = argv[1];
  if (argument.length() < 4) {
    exception_invalid_argument();
  }
  return argument;
}

void exception_invalid_argument() {
  std::cerr << "This executable requires exactly 1 argument.\n"
            << "The argument must either be an absolute path to `*.bag` file "
               "or a serial number\n"
            << "Examples:\n "
            << "./stereo_matching_arp 1234567890\n"
            << "./stereo_matching_arp "
               "/home/best_user/favourite_directory/amazing_recording.bag\n";
  print_connected_devices();
  throw std::runtime_error("Error: Invalid arguments. Shutting down...");
}

void print_connected_devices() {
  rs2::context context;
  rs2::device_list device_list = context.query_devices();
  uint32_t device_count = device_list.size();
  if (!device_count) {
    std::cout << "No device is connected.\n";
  } else {
    std::cout << "The following devices are connected:\n";
    for (uint32_t i = 0; i < device_count; i++) {
      rs2::device device = device_list[i];
      std::cout << device.get_info(RS2_CAMERA_INFO_NAME) << " with serial \""
                << device.get_info(RS2_CAMERA_INFO_SERIAL_NUMBER) << "\"\n";
    }
  }
}

void show_intro_screen(const float &baseline, const float &focal_length) {
  std::cout
      << "\n\n\n\n\n\n\n\n\n------------------------------\nROB768 - Stereo "
         "Matching\nPress spacebar "
         "to match stereo\nPress 'q' to quit\n------------------------------\n";
  std::cout << "Baseline:\t" << baseline << " m\nfocal_length:\t"
            << focal_length << " px\n";
}

void colorize(const cv::Mat1s &input, cv::Mat3b &output, const int &colormap) {
  output = cv::Mat3b(input.size());
  cv::Mat1b normalised;
  cv::normalize(input, normalised, 0, 255, cv::NORM_MINMAX);
  normalised.convertTo(normalised, CV_8UC1);
  cv::applyColorMap(normalised, output, colormap);
}

void black_based_on_mask(const cv::Mat3b &input, cv::Mat3b &output,
                         const cv::Mat1s &mask) {
  output = input.clone();
// Parallelize computations
#pragma omp parallel for
  for (uint16_t r = 0; r < output.rows; ++r) {
    for (uint16_t c = 0; c < output.cols; ++c) {
      if (mask.at<uint16_t>(r, c) == 0) {
        output.at<cv::Vec3b>(r, c)[0] = 0;
        output.at<cv::Vec3b>(r, c)[1] = 0;
        output.at<cv::Vec3b>(r, c)[2] = 0;
      }
    }
  }
}

void imgshow(const cv::String &name, cv::InputArray input) {
  // cv::imwrite(name + ".png", input);
  cv::namedWindow(name, 0);
  cv::imshow(name, input);
}

} // namespace utils
