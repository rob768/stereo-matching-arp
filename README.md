# Stereo Matching (ARP)
Implementation of a stereo matching algorithm, developed for the Advanced Robotic Perception course (ROB7) at Aalborg University.

## Dependencies
- [*CMake*](https://cmake.org/)
- [*OpenCV*](https://opencv.org/)
- [*librealsense*](https://github.com/IntelRealSense/librealsense) (Use an alternative `cv::Mat` source if desired)
- [*OpenMP*](https://openmp.org/) (Optional)

## Build Instructions
Follow the following instructions
```bash
$ git clone https://gitlab.com/rob768/stereo-matching-arp.git && cd stereo-matching-arp
$ mkdir build && cd build
$ cmake ..
$ cmake --build .
```

## Execution
The executable requires exactly 1 argument.
The argument must either be an absolute path to `*.bag` file or a serial number of *D400* device.

Examples:
```bash
$ ./stereo_matching_arp 1234567890
$ ./stereo_matching_arp .../favourite_directory/amazing_recording.bag
```

## Parameters

These parameters can be found under the header for the stereo matching class [here](include/StereoMatcher.hpp). Furthermore, parameters of streams for a *D400* device can be configured [here](include/Rs2Cv.hpp).

|             Property            |   Default Value   |                                                                 Description                                                                |
|:-------------------------------:|:-----------------:|:------------------------------------------------------------------------------------------------------------------------------------------:|
| SOBEL_KERNEL_SIZE               |         3         | The kernel size of the Sobel filter applied to the images in pre-processing. It must be an odd number.                                     |
| WINDOW_SIZE                     |         21        | The window size used for cost computations. It must be an odd number.                                                                      |
| DISPARITY_STEP_SIZE             |         1         | The step size used during computing disparities.                                                                                           |
| MAX_DISPARITY_LEFT              |        100        | The number of pixels to consider the disparity for, to the left of the pixel.                                                              |
| COMPUTE_DISPARITIES_TO_RIGHT    |      Disabled     | Compute disparities also to the right of the pixel.                                                                                        |
| MAX_DISPARITY_RIGHT             |         -         | The number of pixels to consider the disparity for, to the right of the pixel. Applicable only if COMPUTE_DISPARITIES_TO_RIGHT is enabled. |
| USE_SSD_COST                    |      Disabled     | Use SSD instead of SAD.                                                                                                                    |
| MINIMUM_REQUIRED_COST           |  30*WINDOW_SIZE^2 | The minimum required cost for a disparity to be consider a valid match.                                                                    |
| STOP_IF_SATISFACTORY_COST_FOUND |      Enabled      | Stop computing disparity for the full range if cost is less than SATISFACTORY_COST for some disparity.                                     |
| SATISFACTORY_COST               |   WINDOW_SIZE^2   | The satisfactory cost at which computations of disparity for a pixel stop. Applicable only if STOP_IF_SATISFACTORY_COST_FOUND is enabled.  |
| DEPTH_SCALE                     | 1000 (millimeter) | The resulting units of the depth image.                                                                                                    |

## Authors
Group ROB768 (19gr769) with the following members:
- Andrej Orsula (aorsul16@student.aau.dk)
- Eduardo Fonseca (efonse19@student.aau.dk)
