#include "Rs2Cv.hpp"
#include "StereoMatcher.hpp"
#include "utils.hpp"
#include <opencv2/opencv.hpp>

using namespace rs2cv;
using namespace stereo_matcher;
using namespace utils;

/// Define to process images in real time
#define REAL_TIME_PROCESSING

/// The colormap used to colourise depth maps
const int COLORMAP = cv::COLORMAP_JET;

int main(int argc, char *argv[]) try {
  // Parse the command line argument
  std::string argument = parse_cli_argument(argc, argv);

  // Create an instance of `Rs2Cv`
  Rs2Cv rs_to_cv;

  // Initialise `Rs2Cv`
  if (argument.substr(argument.length() - 4) == ".bag") {
    // Initialise from rosbag if arguments ends with ".bag"
    rs_to_cv.init_from_rosbag(argument);
  } else {
    // Else initialise device with its serial number
    rs_to_cv.init_device(argument);
  }

  // Get the baseline and focal length
  float baseline = rs_to_cv.get_baseline();
  float focal_length = rs_to_cv.get_infra_focal_length();

  // Create an instance of `StereoMatcher`
  StereoMatcher stereo_matcher(baseline, focal_length);

  // Print basic info
  show_intro_screen(baseline, focal_length);

  // Loop until user presses the key
  int key = -1;
  while (key != 'q') {
    // Acquire frames
    cv_frameset frameset = rs_to_cv.wait_for_frames();

    // Show the acquired frames
    // imgshow("acquired_realsense_depth", frameset.depth);
    imgshow("acquired_infra_left", frameset.infra_left);
    imgshow("acquired_infra_right", frameset.infra_right);
    // imgshow("acquired_color", frameset.color);

#ifdef REAL_TIME_PROCESSING
    {
      // Clone the images to work with
      cv::Mat1b infra_left = frameset.infra_left.clone();
      cv::Mat1b infra_right = frameset.infra_right.clone();
      // Compute depth
      cv::Mat1s depth;
      stereo_matcher.compute_depth(infra_left, infra_right, depth);
      // Colorize depth
      cv::Mat3b depth_colorized;
      colorize(depth, depth_colorized, COLORMAP);
      black_based_on_mask(depth_colorized, depth_colorized, depth);
      // Show depth
      imgshow("depth_colorized", depth_colorized);
    }
#endif

    // Wait until user presses spacebar or `q`
    key = cv::waitKey(1) & 0xFF;
    if (key != ' ') {
      continue;
    }

    // Clone the images to work with
    cv::Mat1b infra_left = frameset.infra_left.clone();
    cv::Mat1b infra_right = frameset.infra_right.clone();

    // Compute disparity
    cv::Mat1s disparity;
    stereo_matcher.compute_disparity(infra_left, infra_right, disparity);

    // Compute depth
    cv::Mat1s depth;
    stereo_matcher.compute_depth(disparity, depth);

    // Colorize depth
    cv::Mat3b depth_colorized;
    colorize(depth, depth_colorized, COLORMAP);
    black_based_on_mask(depth_colorized, depth_colorized, depth);

    // Normalize disparity
    cv::normalize(disparity, disparity, 0, std::pow(2, 16) - 1,
                  cv::NORM_MINMAX);

    // Normalize depth
    cv::normalize(depth, depth, 0, std::pow(2, 16) - 1, cv::NORM_MINMAX);

    // Display results
    imgshow("disparity", disparity);
    imgshow("depth", depth);
    imgshow("depth_colorized", depth_colorized);

    // Renew intro screen
    show_intro_screen(baseline, focal_length);
  }

  // Return success if everything went alright
  return EXIT_SUCCESS;
} catch (const rs2::error &e) {
  std::cerr << "RealSense error calling " << e.get_failed_function() << "("
            << e.get_failed_args() << "):\n    " << e.what() << std::endl;
  return EXIT_FAILURE;
} catch (const cv::Exception &e) {
  std::cerr << "OpenCV error calling " << e.what() << std::endl;
  return EXIT_FAILURE;
} catch (const std::exception &e) {
  std::cerr << "STD error calling " << e.what() << std::endl;
  return EXIT_FAILURE;
}
