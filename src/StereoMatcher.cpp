#include "StereoMatcher.hpp"

using namespace stereo_matcher;

///////////////////////////
// Public Implementation //
///////////////////////////

StereoMatcher::StereoMatcher(const float &baseline, const float &focal_length) {
  // Multiplication of baseline, focal length and depth scale, which is stored
  // to reduce the number of computations while computing depth from disparity
  baseline_x_focal_length_ = std::round(DEPTH_SCALE * baseline * focal_length);
}

void StereoMatcher::compute_disparity(cv::Mat1b &img_left, cv::Mat1b &img_right,
                                      cv::Mat1s &output) {
  // Set the output format
  output = cv::Mat1s::zeros(img_left.size());

  // Preprocess the images
  preprocess(img_left, img_right);

// Parallelize computations
#pragma omp parallel for
  // Iterature through each row
  for (uint16_t r = (WINDOW_SIZE / 2); r < output.rows - (WINDOW_SIZE / 2);
       ++r) {
    // Get pointer to the current row
    uint16_t *output_row_ptr = output.ptr<uint16_t>(r);

    // Iterate through each column
    for (uint16_t c = (WINDOW_SIZE / 2); c < output.cols - (WINDOW_SIZE / 2);
         ++c) {
      // Compute the disparity for the current pixel
      output_row_ptr[c] = compute_pixel_disparity(img_left, img_right, r, c);
    }
  }
}

void StereoMatcher::compute_depth(const cv::Mat1s &disparity,
                                  cv::Mat1s &output) {
  // Set the output format
  output = cv::Mat1s::zeros(disparity.size());

// Parallelize computations
#pragma omp parallel for
  // Iterature through each row
  for (uint16_t r = (WINDOW_SIZE / 2); r < output.rows - (WINDOW_SIZE / 2);
       ++r) {
    // Get pointers to the current row
    uint16_t *output_row_ptr = output.ptr<uint16_t>(r);
    const uint16_t *disparity_row_ptr = disparity.ptr<const uint16_t>(r);

    // Iterate through each column
    for (uint16_t c = (WINDOW_SIZE / 2); c < output.cols - (WINDOW_SIZE / 2);
         ++c) {
      // Make sure the disparity is valid for the division
      if (disparity_row_ptr[c] != 0) {
        // Compute the depth for the current pixel
        output_row_ptr[c] = baseline_x_focal_length_ / disparity_row_ptr[c];
      }
    }
  }
}

void StereoMatcher::compute_depth(cv::Mat1b &img_left, cv::Mat1b &img_right,
                                  cv::Mat1s &output) {
  // Set the output format
  output = cv::Mat1s::zeros(img_left.size());

  // Preprocess the images
  preprocess(img_left, img_right);

// Parallelize computations
#pragma omp parallel for
  // Iterature through each row
  for (uint16_t r = (WINDOW_SIZE / 2); r < output.rows - (WINDOW_SIZE / 2);
       ++r) {
    // Get pointers to the current row
    uint16_t *output_row_ptr = output.ptr<uint16_t>(r);

    // Iterate through each column
    for (uint16_t c = (WINDOW_SIZE / 2); c < output.cols - (WINDOW_SIZE / 2);
         ++c) {
      // Compute disparity
      const uint16_t disparity =
          compute_pixel_disparity(img_left, img_right, r, c);
      // Make sure the disparity is valid for the division
      if (disparity != 0) {
        // Compute the depth for the current pixel
        output_row_ptr[c] = baseline_x_focal_length_ / disparity;
      }
    }
  }
}

////////////////////////////
// Private Implementation //
////////////////////////////

inline void StereoMatcher::preprocess(cv::Mat1b &img_left,
                                      cv::Mat1b &img_right) {
  cv::Sobel(img_left, img_left, -1, 1, 0, SOBEL_KERNEL_SIZE);
  cv::Sobel(img_right, img_right, -1, 1, 0, SOBEL_KERNEL_SIZE);
}

inline uint16_t StereoMatcher::compute_pixel_disparity(
    const cv::Mat1b &img_left, const cv::Mat1b &img_right, const uint16_t &row,
    const uint16_t &col) {
  // Initialise disparity to 0 (depth at infinity)
  uint16_t disparity = 0;
  // Set minimum required cost
  uint32_t min_cost = MINIMUM_REQUIRED_COST;

  // Get window for the left image
  cv::Mat1b window_left;
  extract_window(img_left, row, col, window_left);

  // Search range
  uint16_t col_min = std::max(col - MAX_DISPARITY_LEFT, (WINDOW_SIZE / 2));
#ifdef COMPUTE_DISPARITIES_TO_RIGHT
  uint16_t col_max =
      std::min(col + MAX_DISPARITY_RIGHT, img_left.cols - (WINDOW_SIZE / 2));
#else
  uint16_t col_max = col - 1;
#endif

  // Iterature over all columns
  for (uint16_t c = col_max; c >= col_min; c = c - DISPARITY_STEP_SIZE) {

    // Get window for the right image
    cv::Mat1b window_right;
    extract_window(img_right, row, c, window_right);

// Compute the cost
#ifndef USE_SSD_COST
    const uint32_t cost = compute_SAD_cost(window_left, window_right);
#else
    const uint32_t cost = compute_SSD_cost(window_left, window_right);
#endif

    // Check if the cost is less than the current minimum
    if (cost <= min_cost) {
      // Compute the disparity
#ifdef COMPUTE_DISPARITIES_TO_RIGHT
      disparity = std::abs(col - c);
#else
      disparity = col - c;
#endif

#ifdef STOP_IF_SATISFACTORY_COST_FOUND
      // Return if the cost is small enough
      if (cost <= SATISFACTORY_COST) {
        return disparity;
      }
#endif
      // Update the cost
      min_cost = cost;
    }
  }

  return disparity;
}

inline void StereoMatcher::extract_window(const cv::Mat1b &img, const int &row,
                                          const int &col, cv::Mat1b &output) {
  cv::Range range_row(row - (WINDOW_SIZE / 2), row + (WINDOW_SIZE / 2));
  cv::Range range_col(col - (WINDOW_SIZE / 2), col + (WINDOW_SIZE / 2));
  output = img(range_row, range_col);
}

#ifndef USE_SSD_COST
inline uint32_t StereoMatcher::compute_SAD_cost(const cv::Mat1b &window_left,
                                                const cv::Mat1b &window_right) {
  // Initiate the cost
  uint32_t cost = 0;
  // Iterate over all rows
  for (uint16_t r = 0; r < window_left.rows; ++r) {
    // Get pointers to the rows
    const uint8_t *window_left_row_ptr = window_left.ptr<const uint8_t>(r);
    const uint8_t *window_right_row_ptr = window_right.ptr<const uint8_t>(r);

    // Iterate over all columns
    for (uint16_t c = 0; c < window_left.cols; ++c) {
      // Add the cost for the current pixel
      cost += std::abs(window_left_row_ptr[c] - window_right_row_ptr[c]);
    }
  }
  return cost;
}

#else

inline uint32_t StereoMatcher::compute_SSD_cost(const cv::Mat1b &window_left,
                                                const cv::Mat1b &window_right) {
  // Initiate the cost
  uint32_t cost = 0;
  // Iterate over all rows
  for (uint16_t r = 0; r < window_left.rows; ++r) {
    // Get pointers to the rows
    const uint8_t *window_left_row_ptr = window_left.ptr<const uint8_t>(r);
    const uint8_t *window_right_row_ptr = window_right.ptr<const uint8_t>(r);

    // Iterate over all columns
    for (uint16_t c = 0; c < window_left.cols; ++c) {
      // Add the cost for the current pixel
      cost += std::pow(window_left_row_ptr[c] - window_right_row_ptr[c], 2);
    }
  }
  return cost;
}
#endif
