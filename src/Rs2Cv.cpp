#include "Rs2Cv.hpp"

using namespace rs2cv;

///////////////////////////
// Public Implementation //
///////////////////////////

Rs2Cv::Rs2Cv() {}

Rs2Cv::~Rs2Cv() { pipeline_.stop(); }

void Rs2Cv::init_from_rosbag(const std::string &file_name,
                             const bool &repeat_playback) {
  rs2::config config;
  config.enable_device_from_file(file_name, repeat_playback);
  pipeline_.start(config);
  determine_enabled_streams();
}

void Rs2Cv::init_device(const std::string &serial) {
  rs2::config config;

  config.enable_device(serial);

  config.disable_all_streams();
  if (DEFAULT_ENABLE_DEPTH) {
    config.enable_stream(RS2_STREAM_DEPTH, DEFAULT_WIDTH_DEPTH,
                         DEFAULT_HEIGHT_DEPTH, FORMAT_DEPTH,
                         DEFAULT_FRAMERATE_DEPTH);
  }
  if (DEFAULT_ENABLE_INFRA_LEFT) {
    config.enable_stream(RS2_STREAM_INFRARED, INDEX_INFRA_LEFT,
                         DEFAULT_WIDTH_INFRA, DEFAULT_HEIGHT_INFRA,
                         FORMAT_INFRA_LEFT, DEFAULT_FRAMERATE_INFRA);
  }
  if (DEFAULT_ENABLE_INFRA_RIGHT) {
    config.enable_stream(RS2_STREAM_INFRARED, INDEX_INFRA_RIGHT,
                         DEFAULT_WIDTH_INFRA, DEFAULT_HEIGHT_INFRA,
                         FORMAT_INFRA_RIGHT, DEFAULT_FRAMERATE_INFRA);
  }
  if (DEFAULT_ENABLE_COLOR) {
    config.enable_stream(RS2_STREAM_COLOR, DEFAULT_WIDTH_COLOR,
                         DEFAULT_HEIGHT_COLOR, FORMAT_COLOR,
                         DEFAULT_FRAMERATE_COLOR);
  }

  pipeline_.start(config);
  determine_enabled_streams();
}

void Rs2Cv::init_device(const std::string &serial, rs2::config &config) {
  config.enable_device(serial);
  pipeline_.start(config);
  determine_enabled_streams();
}

cv_frameset Rs2Cv::wait_for_frames() {
  cv_frameset cv_frameset;
  rs2::frameset rs2_frameset = pipeline_.wait_for_frames();

  if (enabled_streams_.depth) {
    extract_depth_frame(rs2_frameset, cv_frameset.depth);
  }
  if (enabled_streams_.infra_left) {
    extract_infra_left_frame(rs2_frameset, cv_frameset.infra_left);
  }
  if (enabled_streams_.infra_right) {
    extract_infra_right_frame(rs2_frameset, cv_frameset.infra_right);
  }
  if (enabled_streams_.color) {
    extract_color_frame(rs2_frameset, cv_frameset.color);
  }

  return cv_frameset;
}

float Rs2Cv::get_baseline() {
  rs2::pipeline_profile pipeline_profile = pipeline_.get_active_profile();
  rs2::stream_profile left_infra_stream_profile =
      pipeline_profile.get_stream(RS2_STREAM_INFRARED, INDEX_INFRA_LEFT);
  rs2::stream_profile right_infra_stream_profile =
      pipeline_profile.get_stream(RS2_STREAM_INFRARED, INDEX_INFRA_RIGHT);
  rs2_extrinsics extrinsics =
      right_infra_stream_profile.get_extrinsics_to(left_infra_stream_profile);
  float baseline = extrinsics.translation[0];
  return baseline;
}

float Rs2Cv::get_infra_focal_length(const int &sensor_index) {
  rs2::pipeline_profile pipeline_profile = pipeline_.get_active_profile();
  rs2::stream_profile infra_stream_profile =
      pipeline_profile.get_stream(RS2_STREAM_INFRARED, sensor_index);
  rs2_intrinsics intrinsics =
      infra_stream_profile.as<rs2::video_stream_profile>().get_intrinsics();
  float focal_length = intrinsics.fx;
  return focal_length;
}

////////////////////////////
// Private Implementation //
////////////////////////////

inline void Rs2Cv::determine_enabled_streams() {
  rs2::pipeline_profile pipeline_profile = pipeline_.get_active_profile();
  std::vector<rs2::stream_profile> streams = pipeline_profile.get_streams();
  for (rs2::stream_profile &stream : streams) {
    switch ((int)stream.stream_type()) {
    case RS2_STREAM_DEPTH: {
      enabled_streams_.depth = true;
      break;
    }
    case RS2_STREAM_INFRARED: {
      if (stream.stream_index() == INDEX_INFRA_LEFT) {
        enabled_streams_.infra_left = true;
      } else if (stream.stream_index() == INDEX_INFRA_RIGHT) {
        enabled_streams_.infra_right = true;
      }
      break;
    }
    case RS2_STREAM_COLOR: {
      enabled_streams_.color = true;
      break;
    }
    }
  }
}

inline void Rs2Cv::extract_depth_frame(const rs2::frameset &rs2_frameset,
                                       cv::Mat1s &output) {
  convert_frame_to_cv_mat(rs2_frameset.get_depth_frame(), CV_16UC1, output);
}

inline void Rs2Cv::extract_infra_left_frame(const rs2::frameset &rs2_frameset,
                                            cv::Mat1b &output) {
  convert_frame_to_cv_mat(rs2_frameset.get_infrared_frame(INDEX_INFRA_LEFT),
                          CV_8UC1, output);
}

inline void Rs2Cv::extract_infra_right_frame(const rs2::frameset &rs2_frameset,
                                             cv::Mat1b &output) {
  convert_frame_to_cv_mat(rs2_frameset.get_infrared_frame(INDEX_INFRA_RIGHT),
                          CV_8UC1, output);
}

inline void Rs2Cv::extract_color_frame(const rs2::frameset &rs2_frameset,
                                       cv::Mat3b &output) {
  convert_frame_to_cv_mat(rs2_frameset.get_color_frame(), CV_8UC3, output);
}

inline void Rs2Cv::convert_frame_to_cv_mat(const rs2::frame &frame,
                                           const int &mat_type,
                                           cv::Mat &output) {
  const int width = frame.as<rs2::video_frame>().get_width();
  const int height = frame.as<rs2::video_frame>().get_height();
  output = cv::Mat(cv::Size(width, height), mat_type, (void *)frame.get_data());
}
